package fi.vamk.e1801137;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
    
    public static String helloWeekday(String day) {
    	Date date = null;
    	if(day == null || day.equals("")) {
    		return "Hello World!";
    	}
		try {
			date = new SimpleDateFormat("d/M/yyyy").parse(day);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		String dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date);
    	
    	return "Hello " + dayOfWeek + " World!";
    }
}
