package fi.vamk.e1801137;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void WeekdayTest1()
    {
    	String test = App.helloWeekday("16/03/2020");
    	assertEquals("Hello Monday World!", test);
    }
    
    @Test
    public void WeekdayTest2()
    {
    	String test = App.helloWeekday("17/03/2020");
    	assertEquals("Hello Tuesday World!", test);
    }
    
    @Test
    public void WeekdayTest3()
    {
    	String test = App.helloWeekday("");
    	assertEquals("Hello World!", test);
    }
    
    @Test
    public void WeekdayTest4()
    {
    	String test = App.helloWeekday(null);
    	assertEquals("Hello World!", test);
    }
}
